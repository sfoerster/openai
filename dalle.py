#!/usr/bin/env python3

import os
import platform
import subprocess
import openai
import requests
import random
import string
from dotenv import load_dotenv

load_dotenv()

# Load your API key from an environment variable or secret management service
openai.api_key = os.getenv("OPENAI_API_KEY")

print()
prompt = input("Text prompt for image generation (e.g. 'A blue eagle flying over a desert at sunset'): ")
print()

print("Sending to OpenAI...")
print()

response = openai.Image.create(
  prompt=prompt,
  n=1,
  size="1024x1024"
)

image_url = response['data'][0]['url']

print(f"Image URL: {image_url}")
print()

# URL of the image to be downloaded is defined as image_url
r = requests.get(image_url) # create HTTP response object

# send a HTTP request to the server and save
# the HTTP response in a response object called r
fname = f"{''.join([c for c in prompt.strip().replace(' ','_') if c.isalnum() or c == '_'])}.png"

if not os.path.exists("images"):
    os.mkdir("images")

if os.path.isfile(os.path.join("images", fname)):
    fname = fname.split(".")[0] + f".{''.join(random.choice(string.ascii_letters) for x in range(5))}.png"

fname = os.path.join("images", fname)
print(f"Filename: {fname}")
with open(fname, 'wb') as f:

    # Saving received content as a png file in
    # binary format

    # write the contents of the response (r.content)
    # to a new file in binary mode.
    f.write(r.content)

if platform.system() == 'Darwin':       # macOS
    subprocess.call(('open', fname))
elif platform.system() == 'Windows':    # Windows
    os.startfile(fname)
else:                                   # linux variants
    subprocess.call(('xdg-open', fname))
