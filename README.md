OpenAI Python API code

[[_TOC_]]

# Quickstart

## Git clone

```
git clone https://gitlab.com/sfoerster/openai
cd openai
```

## (Optional) Setup Python virtual environment

### PyENV

https://github.com/pyenv/pyenv#getting-pyenv

```
pyenv install 3.10.8
pyenv virtualenv 3.10.8 envopenai
pyenv activate envopenai
pip install -U pip
```

## Project Environment

(Assuming you are in the `openai` folder)

```
pip install -r requirements.txt

cp .env.example .env
```

## Retrieve API key from https://beta.openai.com/account/api-keys

Copy your API key into `.env`

.env
```
OPENAI_API_KEY=sk-................
```

## Generate some images

```
python dalle.py
Text prompt for image generation (e.g. 'A blue eagle flying over a desert at sunset'): cloaked mistborn flying over medieval fortree with translucent computer code on a blue background

Sending to OpenAI...
Image URL: https://oaidalleapiprodscus.blob.core.windows.net/private/org-..................

Filename: images/cloaked_mistborn_flying_over_medieval_fortree_with_translucent_computer_code_on_a_blue_background.png
```
